#include "ros_msg_parser/ros_parser.hpp"
#include <ros/ros.h>

#include <vector>
#include <chrono>
#include <pqxx/pqxx>
#include <libpq-fe.h>
#include <getopt.h>
#include <boost/thread/thread.hpp>
#include "libpq-pool/pgbackend.h"
#include "boost/thread.hpp"
#include <city.h>

using namespace std::chrono;

int32_t uint32_to_int32(uint32_t value)
{
	int32_t tmp;
	std::memcpy(&tmp, &value, sizeof(tmp));
	return value;//tmp;
}

char *myhton(char *src, int size) {
  char *dest = (char *)malloc(sizeof(char) * size);
  switch (size) {
  case 1:
    *dest = *src;
    break;
  case 2:
    *(int16_t *)dest = htobe16(*(int16_t *)src);
    break;
  case 4:
    *(int32_t *)dest = htobe32(*(int32_t *)src);
    break;
  case 8:
    *(int64_t *)dest = htobe64(*(int64_t *)src);
    break;
  default:
    *dest = *src;
    break;
  }
  memcpy(src, dest, size);
  free(dest);
  return src;
}

void topicCallback(const RosMsgParser::ShapeShifter& msg,
                   const std::string& topic_name,
                   const std::vector<std::string>& sub_topic_names_list,
                   const bool& list_sub_topics,
                   const std::string& target_table_name,
                   const int32_t type_hash,
                   RosMsgParser::ParsersCollection& parsers,
                   PGBackend& pgbackend)
{
  // you must register the topic definition.
  //  Don't worry, it will not be done twice
  parsers.registerParser(topic_name, msg);

  auto deserialized_msg = parsers.deserialize(topic_name, msg);

  high_resolution_clock::time_point t1 = high_resolution_clock::now();

  std::vector<int32_t> id;
  std::vector<int64_t> val;

  // Print the content of the message
  // printf("--------- %s ----------\n", topic_name.c_str());
  
  for (auto it : deserialized_msg->renamed_vals)
  {
    const std::string& key = it.first;
    const double value = it.second;
    // std::cout << "key: " << key << " value: " << value << std::endl;
    if(list_sub_topics)
    {
      printf(" %s (%i) = %f\n", key.c_str(), uint32_to_int32( CityHash32( key.c_str(), key.size())), value);
    }
    if (std::find(sub_topic_names_list.begin(), sub_topic_names_list.end(), key) == sub_topic_names_list.end() )
    {
      continue;
    }
    std::cout << "key: " << key << " id: " << uint32_to_int32( CityHash32( key.c_str(), key.size())) << " value: " << value << std::endl;
    id.push_back( uint32_to_int32( CityHash32( key.c_str(), key.size())) );
    val.push_back( (int64_t)(value * 1000000) );
  }
  for (auto it : deserialized_msg->flat_msg.name)
  {
    const std::string& key = it.first.toStdString();
    const std::string& value = it.second;
    // std::cout << "key: " << key << " value: " << value << std::endl;
    if(list_sub_topics)
    {
      printf(" %s (%i) = %s\n", key.c_str(), uint32_to_int32( CityHash32( key.c_str(), key.size())), value.c_str());
    }
    // if (std::find(sub_topic_names_list.begin(), sub_topic_names_list.end(), key) == sub_topic_names_list.end())
    if (std::find(sub_topic_names_list.begin(), sub_topic_names_list.end(), key) == sub_topic_names_list.end() )
    {
      continue;
    }
    std::string str_defloat = boost::lexical_cast<std::string>(value);
    if (str_defloat.find(".") > -1)
    {
		  str_defloat = str_defloat.replace(str_defloat.find("."),1,"");
    }
		std::istringstream iss(str_defloat);
    int64_t tval;
		iss >> tval;
    std::cout << "key: " << key << " id: " << uint32_to_int32( CityHash32( key.c_str(), key.size())) << " value: " << value << std::endl;
    id.push_back( uint32_to_int32( CityHash32( key.c_str(), key.size())) );
    val.push_back( tval );    
  }

  if(list_sub_topics)
  {
    std::cout << "Listing sub-topics only. Exiting..." << std::endl;
    exit(0);
  }

/*
* build postgres binary buffer
*/
  // int32_t type_hash = 0;
  char header[12] = "PGCOPY\n\377\r\n\0";
  int row_count = id.size();
  if( row_count == 0 )
  {
    std::cout << "No data to read" << std::endl;
    return;
  }
  int buf_pos = 0;
  int flag = 0;
  int extension = 0;

  int buffer_size = (((row_count) * 30)+21);

  char buffer[buffer_size];
  int size = 0;
  buf_pos += 11;

  memcpy(buffer, header, buf_pos);		
  memcpy(&buffer[buf_pos], (void *)&flag, 4);
  buf_pos += 4;

  memcpy(&buffer[buf_pos], (void *)&extension, 4);
  buf_pos += 4;
  
  short fieldnum = 0;
  char buff[21];

  for(int i = 0, l = row_count; i < l; i++) 
  {

    fieldnum = 3;
    memcpy(&buffer[buf_pos], myhton((char *)&fieldnum, 2), 2);
    buf_pos += 2;

    size = 8;
    memcpy(&buffer[buf_pos], myhton((char *)&size, 4), 4);
    buf_pos += 4;

    memcpy(&buffer[buf_pos], myhton((char *)&val.at(i), 8), 8);
    buf_pos += 8;

    // type
    size = 4;
    memcpy(&buffer[buf_pos], myhton((char *)&size, 4), 4);
    buf_pos += 4;
    int32_t th = type_hash;
    memcpy(&buffer[buf_pos], myhton((char *)&th, 4), 4);
    buf_pos += 4;

    // id: hash of x + y
    size = 4;
    memcpy(&buffer[buf_pos], myhton((char *)&size, 4), 4);
    buf_pos += 4;

    memcpy(&buffer[buf_pos], myhton((char *)&id.at(i), 4), 4);
    buf_pos += 4;

  }

  id.clear();
  val.clear();

// 	// end of file
  short negative = -1;
  memcpy(&buffer[buf_pos], myhton((char *)&negative, 2), 2);
  
  /*
  * write buffer to postgres using copy from stdin
  */

  std::shared_ptr<PGConnection> conn = pgbackend.connection();
  PGresult *res = NULL;

  std::string sql_copy = "COPY " + target_table_name + " (v, t, i) FROM STDIN (FORMAT binary);";

  res = PQexec(conn->connection().get(), sql_copy.c_str());
  if (PQresultStatus(res) != PGRES_COPY_IN)
  {
    std::cout << "Not in COPY_IN mode";
    PQclear(res);
  }
  else
  {
    PQclear(res);
    int copyRes = PQputCopyData(conn->connection().get(), buffer, buffer_size);
    if (copyRes == 1)
    {
      if (PQputCopyEnd(conn->connection().get(), NULL) == 1)
      {
        res = PQgetResult(conn->connection().get());
        if (PQresultStatus(res) != PGRES_COMMAND_OK)
        {
          std::cout << PQerrorMessage(conn->connection().get()) << std::endl;
        }
        PQclear(res);
      }
      else
      {
        std::cout << PQerrorMessage(conn->connection().get()) << std::endl;
      }
    }
    else if (copyRes == 0)
    {
      std::cout << "Send no data, connection is in nonblocking mode" << std::endl;
    }
    else if (copyRes == -1)
    {
      std::cout << "Error occur: " << PQerrorMessage(conn->connection().get()) << std::endl;
    }
  }
  pgbackend.freeConnection(conn);

  high_resolution_clock::time_point t2 = high_resolution_clock::now();
	auto transaction_total_duration = duration_cast<milliseconds>( t2 - t1 ).count();
	std::cout << "generic_ros_postgres " << std::to_string(row_count) << " rows transaction total duration: " << transaction_total_duration << std::endl;

}



// usage: pass the name of the topic as command line argument
int main(int argc, char** argv)
{
  int thread_count = 4;
	int tick_rate = 10;
	int opt = 0;
  bool list_sub_topics = false;
  
	std::string topic_name = ""; 
  std::stringstream sub_topic_names;
  std::vector<std::string> sub_topic_names_list;

	std::string postgres_host_ip = "";
	std::string postgres_port = "5432";
	std::string postgres_db_name = "postgres";
	std::string postgres_user = "postgres";
	std::string postgres_password = "password";
  std::string postgres_schemas = "public";

	std::string container_id = "";
  std::string target_table_name = "input_stream";
  
	const char *opts = "+"; // set "posixly-correct" mode
	const option longopts[]{
		{"postgres_host_ip", 1, 0, 'a'},
		{"postgres_port", 1, 0, 'b'},
		{"postgres_db_name", 1, 0, 'c'},
		{"postgres_user", 1, 0, 'd'},
		{"postgres_password", 1, 0, 'e'},
		{"container_id", 1, 0, 'f'},
		{"topic_name", 1, 0, 'g'},
		{"thread_count", 1, 0, 'h'},
		{"tick_rate", 1, 0, 'i'},
    {"list_sub_topics", 1, 0, 'j'},
    {"sub_topic_names", 1, 0, 'k'},
    {"postgres_schemas", 1, 0, 'l'},
    {"target_table_name", 1, 0, 'm'},
		{0, 0, 0, 0}}; 

	while ((opt = getopt_long_only(argc, argv, opts, longopts, 0)) != -1)
	{
		switch (opt)
		{
		case 'a':
			std::cout << "setting postgres_host_ip: " << optarg << std::endl;
			postgres_host_ip = optarg;
			break;
		case 'b':
			std::cout << "setting postgres_port: " << optarg << std::endl;
			postgres_port = optarg;
			break;
		case 'c':
			std::cout << "setting postgres_db_name: " << optarg << std::endl;
			postgres_db_name = optarg;
			break;
		case 'd':
			std::cout << "setting postgres_user: " << optarg << std::endl;
			postgres_user = optarg;
			break;
		case 'e':
			std::cout << "setting postgres_password: " << optarg << std::endl;
			postgres_password = optarg;
			break;
		case 'f':
			std::cout << "setting container_id: " << optarg << std::endl;
			container_id = optarg;
			break;
		case 'g':
			std::cout << "setting topic_name: " << optarg << std::endl;
			topic_name = optarg;
			break;
		case 'h':
			std::cout << "setting thread count: " << optarg << std::endl;
			thread_count = atoi(optarg);
			break;
		case 'i':
			std::cout << "setting tick Hz rate: " << optarg << std::endl;
			tick_rate = atoi(optarg);
			break;
    case 'j':
			std::cout << "list sub topics: " << optarg << std::endl;
			list_sub_topics = ( ( (std::string)optarg ).compare("true") == 0);
			break;
    case 'k':
			std::cout << "setting sub topic names: " << optarg << std::endl;
			sub_topic_names << optarg;
      while( sub_topic_names.good() )
      {
          std::string substr;
          getline( sub_topic_names, substr, ',' );
          sub_topic_names_list.push_back( substr );
      }
			break;
    case 'l':
			std::cout << "setting postgres_schemas: " << optarg << std::endl;
			postgres_schemas = optarg;
			break;
    case 'm':
			std::cout << "setting target_table_name: " << optarg << std::endl;
			target_table_name = optarg;
			break;
		default: 
			exit(EXIT_FAILURE);
		}
	}

	std::cout << "Starting..." << std::endl;

	std::string ros_node_name = "generic_ros_postgres_" + container_id;

	ros::init(argc, argv, ros_node_name);
	ros::NodeHandle nh;

	ros::master::V_TopicInfo topic_infos;
  ros::master::getTopics(topic_infos);
	std::cout << "ROS topics: " << std::endl;
	for(int i, l = topic_infos.size(); i < l; i++) {
		std::cout << "topic name: " << topic_infos[i].name << " topic type: " << topic_infos[i].datatype << std::endl;
	}
	// defaults to Unix socket - 30% faster than TCP
	std::string db_conn_str = "dbname='" + postgres_db_name + "' user='" + postgres_user + "' password='" + postgres_password + "' options='-c search_path=" + postgres_schemas + "'";
	// only if host IP specified then switch to TCP
	if(postgres_host_ip.length() > 0)
	{
		db_conn_str = "postgresql://" + postgres_user + ":" + postgres_password + "@" + postgres_host_ip + ":" + postgres_port + "/" + postgres_db_name + "?options=-c%20search_path%3D" + postgres_schemas;
	}
	std::cout << db_conn_str << std::endl;
	char conninfo[db_conn_str.size() + 1];
	strcpy(conninfo, db_conn_str.c_str());
	
	std::shared_ptr<PGBackend> pgbackend;
  pgbackend = std::make_shared<PGBackend>(db_conn_str, thread_count);

  int32_t type_hash = 0;
  for(int i = 0, l = topic_infos.size(); i < l; i++) 
  {
    if((topic_infos[i].name).find(topic_name) != std::string::npos)
    {
      type_hash = uint32_to_int32( CityHash32(topic_infos[i].datatype.c_str(), ((size_t)sizeof(topic_infos[i].datatype.size()))) );
    }
  }
  
  std::cout << "type_hash: " << std::to_string(type_hash) << std::endl;

  RosMsgParser::ParsersCollection parsers;

  // who is afraid of lambdas and boost::functions ? (I am!)
  boost::function<void(const RosMsgParser::ShapeShifter::ConstPtr&)> callback;
  callback = [&parsers, topic_name, sub_topic_names_list, list_sub_topics, target_table_name, type_hash, &pgbackend](const RosMsgParser::ShapeShifter::ConstPtr& msg) -> void {
    topicCallback(*msg, topic_name, sub_topic_names_list, list_sub_topics, target_table_name, type_hash, parsers, *pgbackend);
  };
  ros::Subscriber subscriber = nh.subscribe(topic_name, tick_rate, callback);

  ros::spin();
  return 0;
  
}
