FROM ubuntu:16.04

ENV DEBIAN_FRONTEND noninteractive

RUN mkdir -p /generic_ros_postgres

WORKDIR /generic_ros_postgres

# Build tools
RUN apt update && \
    apt install -y \
    sudo \
    tzdata \
    git \
    cmake \
    cmake-curses-gui \
    wget \
    unzip \
    build-essential \ 
    rsync \
    lsb-release \
    curl

# Media I/O:
RUN apt install -y \
    zlib1g-dev \
    libjpeg-dev \
    libwebp-dev \
    libpng-dev \
    libtiff5-dev \
    libopenexr-dev \
    libgdal-dev \
    libgtk2.0-dev

# Video I/O:
RUN apt install -y \
    libdc1394-22-dev \
    libavcodec-dev \
    libavformat-dev \
    libswscale-dev \
    libtheora-dev \
    libvorbis-dev \
    libxvidcore-dev \
    libx264-dev \
    yasm \
    libopencore-amrnb-dev \
    libopencore-amrwb-dev \
    libv4l-dev \
    libxine2-dev \
    libgstreamer1.0-dev \
    libgstreamer-plugins-base1.0-dev

# Parallelism and linear algebra libraries:
RUN apt install -y \
    libtbb-dev \
    libeigen3-dev

# Python:
RUN apt install -y \
    python3-dev \
    python3-tk \
    python3-numpy

RUN apt install -y \
    libprotobuf-dev \
    protobuf-compiler \
    # libprotobuf-lite10 \
    # libprotobuf10 \
    libprotoc-dev \
    python-protobuf \
    # python3-protobuf \
    libyaml-cpp-dev 

# # ROS
# RUN wget https://github.com/ApolloAuto/apollo-platform/releases/download/2.1.2/ros-indigo-apollo-2.1.2-x86_64.tar.gz \
# && tar zxvf ros-indigo-apollo-2.1.2-x86_64.tar.gz \
# && mkdir -p /apollo/third_party/ros_x86_64 \
# && rsync -av ros/ /apollo/third_party/ros_x86_64 \
# && echo 'source /apollo/third_party/ros_x86_64/setup.bash' >> ~/.bashrc

# # ROSBridge
# RUN apt install -y lsb-release  \
# && echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list \
# &&  apt-key del 421C365BD9FF1F717815A3895523BAEEB01FA116 \
# && apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654 \
# && apt update \
# && apt install -y ros-melodic-rosbridge-server

#ROS python generator workaround
RUN apt-get update && apt-get install --yes python-rosinstall-generator software-properties-common

RUN add-apt-repository restricted \
&& add-apt-repository multiverse \
&& add-apt-repository universe
#ROS

RUN apt-get install -y lsb-core \
    && sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list' \
    && apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654 \
    && apt-get update \
    && apt-get install -y ros-kinetic-desktop-full \
    && echo "source /opt/ros/kinetic/setup.bash" >> ~/.bashrc \
    # && source ~/.bashrc \
    && apt install -y python-rosdep python-rosinstall python-rosinstall-generator python-wstool build-essential \
    && apt install python-rosdep \
    && rosdep init \
    && rosdep update 

# PostgreSQL 12 Headers
RUN apt install -y postgresql-common \
    # && yes | sh /usr/share/postgresql-common/pgdg/apt.postgresql.org.sh \
    && apt install -y --allow-unauthenticated libpqxx-dev libpq-dev postgresql-server-dev-all


# Install gcc 7
RUN apt update -qq \
&& apt install -yq software-properties-common \
&& add-apt-repository -y ppa:ubuntu-toolchain-r/test \
&& apt update -qq \
&& apt install -yq g++-7 \
&& apt clean \
&& rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
# Configure alias
RUN update-alternatives \
 --install /usr/bin/gcc gcc /usr/bin/gcc-7 60 \
 --slave /usr/bin/g++ g++ /usr/bin/g++-7 \
 --slave /usr/bin/gcov gcov /usr/bin/gcov-7 \
 --slave /usr/bin/gcov-tool gcov-tool /usr/bin/gcov-tool-7 \
 --slave /usr/bin/gcc-ar gcc-ar /usr/bin/gcc-ar-7 \
 --slave /usr/bin/gcc-nm gcc-nm /usr/bin/gcc-nm-7 \
 --slave /usr/bin/gcc-ranlib gcc-ranlib /usr/bin/gcc-ranlib-7


# # GCC7
# RUN apt-get install -y software-properties-common \
#     # && add-apt-repository ppa:ubuntu-toolchain-r/test \
#     && apt update \
#     && apt install g++-7 -y \
#     && update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-7 60 \
#     --slave /usr/bin/g++ g++ /usr/bin/g++-7 \
#     && update-alternatives --config gcc

WORKDIR /opt
# VCPKG
RUN git clone https://github.com/Microsoft/vcpkg.git \
    && cd vcpkg \
    && ./bootstrap-vcpkg.sh \
    && ./vcpkg integrate install \
    && ln -s /opt/vcpkg/vcpkg /usr/bin/vcpkg

# morton-ndXXHASH
RUN vcpkg install morton-nd

# CMAKE 3.17
RUN wget https://github.com/Kitware/CMake/releases/download/v3.17.0/cmake-3.17.0.tar.gz \
    && tar xvfz cmake-3.17.0.tar.gz \
    && cd cmake-3.17.0 \
    && ./configure \
    && make -j$(nproc) \
    && make install

ENV CMAKE_PREFIX_PATH ${CMAKE_PREFIX_PATH}:/opt/vcpkg/installed/x64-linux/share

# cityhash
RUN apt-get install -y autotools-dev && git clone https://github.com/google/cityhash.git \
&& cd cityhash/ \
&& ./configure --enable-sse4.2 \
&& make -j$(nproc) all check CXXFLAGS="-g -O3 -msse4.2" \
&& make install

# COPY cmake/cityhashConfig.cmake /usr/share/cmake-3.5/Modules/Findcityhash.cmake
COPY cmake/cityhashConfig.cmake /usr/local/share/cmake-3.17/Modules/Findcityhash.cmake

# TFMessage ROS Postgres
RUN . /opt/ros/kinetic/setup.sh \
    && git clone --recurse-submodules https://bitbucket.org/dammian_miller/generic_ros_postgres.git \
    && cd generic_ros_postgres \
    && mkdir -p build && cd build \
    && cmake ..       \
    &&   make -j$(nproc)

# RUN ln -s /usr/lib/x86_64-linux-gnu/libconsole_bridge.so.0.4 /usr/lib/libconsole_bridge.so.0.2 \
# && ln -s /usr/lib/x86_64-linux-gnu/libboost_system.so.1.65.1 /usr/lib/x86_64-linux-gnu/libboost_system.so.1.54.0 \
# && ln -s /usr/lib/x86_64-linux-gnu/libboost_thread.so.1.65.1 /usr/lib/x86_64-linux-gnu/libboost_thread.so.1.54.0 \
# && ln -s /usr/lib/x86_64-linux-gnu/libboost_filesystem.so.1.65.1 /usr/lib/x86_64-linux-gnu/libboost_filesystem.so.1.54.0 \
# && ln -s /usr/lib/x86_64-linux-gnu/libboost_regex.so.1.65.1 /usr/lib/x86_64-linux-gnu/libboost_regex.so.1.54.0

WORKDIR /opt/generic_ros_postgres
RUN chmod +x run.sh
SHELL ["/bin/bash", "-c"]
CMD ["/bin/bash", "-c", "./run.sh"]

