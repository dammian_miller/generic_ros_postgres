#ifndef PGBACKEND_H
#define PGBACKEND_H

#include <memory>
#include <mutex>
#include <string>
#include <queue>
#include <stdio.h>
#include <iostream>
#include <thread>
#include <fstream>
#include <sstream>
#include "pgconnection.h"
#include <condition_variable>
#include <libpq-fe.h>

class PGBackend
{
public:
    PGBackend(std::string &db_conn_str_, int &db_pool_size_):db_conn_str(db_conn_str_), POOL(db_pool_size_)
    {
        char tmpconninfo[db_conn_str_.size() + 1];
	    strcpy(tmpconninfo, db_conn_str_.c_str());
        conninfo = tmpconninfo;
        std::cout << "conninfo: " << conninfo << std::endl;
        createPool();
    }
    std::shared_ptr<PGConnection> connection()
    {

        std::unique_lock<std::mutex> lock_( m_mutex );

        while ( m_pool.empty() ){
                m_condition.wait( lock_ );
        }

        auto conn_ = m_pool.front();
        m_pool.pop();

        return  conn_;
    }
    void freeConnection(std::shared_ptr<PGConnection> conn_)
    {
        std::unique_lock<std::mutex> lock_( m_mutex );
        m_pool.push( conn_ );
        lock_.unlock();
        m_condition.notify_one();
    }

private:
    void createPool()
    {
        std::lock_guard<std::mutex> locker_( m_mutex );
        std::cout << "conninfo: " << conninfo << std::endl;
        for ( auto i = 0; i< POOL; ++i ){
            m_pool.emplace ( std::make_shared<PGConnection>(conninfo) );
        }
    }

    std::mutex m_mutex;
    std::condition_variable m_condition;
    std::queue<std::shared_ptr<PGConnection>> m_pool;

    int POOL = 10;

    std::string db_conn_str;
    char *conninfo;

};

#endif //PGBACKEND_H