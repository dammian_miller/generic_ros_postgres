#!/bin/bash
set -e
echo "export CONTAINER_ID=`head -1 /proc/self/cgroup|cut -d/ -f3`" >> /root/.bashrc
source /opt/ros/kinetic/setup.bash
sleep ${START_DELAY_WAIT:-0}
./devel/lib/generic_ros_postgres/generic_subscriber --topic_name=${TOPIC_NAME:-/X1/imu/data} --container_id=${CONTAINER_ID:-1} --thread_count=${THREAD_COUNT:-3} --postgres_host_ip=${POSTGRES_HOST_IP:-} --postgres_port=${POSTGRES_PORT:-} --postgres_user=${POSTGRES_USER:-postgres} --postgres_password=${POSTGRES_PASSWORD:-password} --postgres_db_name=${POSTGRES_DB_NAME:-postgres} --tick_rate=${TICK_RATE:-50} --list_sub_topics=${LIST_SUB_TOPICS:-false} --sub_topic_names=${SUB_TOPIC_NAMES:-} --target_table_name=${TARGET_TABLE_NAME:-input_stream} --postgres_schemas=${POSTGRES_SCHEMAS:-cognition} --target_table_name=${TARGET_TABLE_NAME:-input_stream} 
